// *********************** DELETE VALUES IN ARRAY */ *********************** //

function handleFormSubmit(e) {
  e.preventDefault();
}

let deleteValueBtns = document.querySelectorAll(".delete_value");
let arr = [23, 4, 38, 65, 34, 29];

// *** Delete Value at first and last position *** //
deleteValueBtns.forEach((btn) => {
  btn.addEventListener("click", () => {
    if (arr.length <= 0) {
      document.querySelector(".display_insert_array").innerText =
        "Empty Array, kindly reset!";
      return;
    }

    if (btn.innerText.toLowerCase() === "pop") {
      arr.pop();
      document.querySelector(
        ".display_insert_array"
      ).innerText = `Array:- [${arr}]`;

      return;
    }

    if (btn.innerText.toLowerCase() === "shift") {
      arr.shift();
      document.querySelector(
        ".display_insert_array"
      ).innerText = `Array:- [${arr}]`;

      return;
    }

    if (btn.innerText.toLowerCase() === "reset") {
      arr = [23, 4, 38, 65, 34, 29];
      document.querySelector(
        ".display_insert_array"
      ).innerText = `Array:- [${arr}]`;
      return;
    }
  });
});

// *** Delete nth Value from the Array *** //
let position = document.querySelector(".delete_index");
let displayedArray = document.querySelector(".display_delete_array");
let deletedBtns = document.querySelectorAll(".delete_value_btn");
let arr1 = [23, 4, 38, 65, 34, 29];

function handleDeleteFormSubmit(e) {
  e.preventDefault();
}

deletedBtns.forEach((btn) => {
  btn.addEventListener("click", (e) => {
    if (e.target.innerText.toLowerCase() === "delete") {
      let index = position.value;

      if (index === "") {
        alert("All fields are mandatory!");
        return;
      }

      if (index > arr1.length - 1) {
        alert("Index is greater than Array length!");
        return;
      }

      index = parseInt(index);
      arr1 = [...arr1.slice(0, index), ...arr1.splice(index + 1)];

      displayedArray.innerText = `Array:- [${arr1}]`;
    }

    if (e.target.innerText.toLowerCase() === "reset") {
      arr1 = [23, 4, 38, 65, 34, 29];
      displayedArray.innerText = `Array:- [${arr1}]`;
    }
  });
});
