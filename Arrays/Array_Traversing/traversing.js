// *********************** ARRAY TRAVERSING *********************** //

function handleFormSubmit(e) {
  e.preventDefault();
}

let getIndex = document.querySelector(".input_num");
let output = document.querySelector(".output");
let getValueBtn = document.querySelector(".get_value");

let arr = [32, 12, 11, 93, 3, 2];

/* document.write("[");
for (let i = 0; i < arr.length; i++) {
  console.log(arr[i]);
  document.write(`${arr[i]},`);
}
document.write("]"); */

getValueBtn.addEventListener("click", () => {
  if (getIndex.value >= arr.length) {
    output.innerText = `Please enter the values between "0" to "5"`;
    return;
  } else if (getIndex.value < 0) {
    output.innerText = `Please enter positive integer value!`;
    return;
  } else if (getIndex.value === "") {
    output.innerText = `Please enter value!`;
    return;
  } else {
    output.innerText = arr[getIndex.value];
  }
});
