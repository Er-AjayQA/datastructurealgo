// *********************** INSERT VALUES IN ARRAY */ *********************** //

function handleFormSubmit(e) {
  e.preventDefault();
}

let insertValue = document.querySelector(".input_insert_value");
let insertValueBtns = document.querySelectorAll(".insert_value");

let arr = [];

// *** Insert Value at first and last position *** //
insertValueBtns.forEach((btn) => {
  btn.addEventListener("click", () => {
    let value = insertValue.value;

    if (value === "") {
      alert("Please enter a value");
      return;
    }

    if (btn.innerText.toLowerCase() === "push") {
      arr.push(value);
      document.querySelector(
        ".display_insert_array"
      ).innerText = `Array:- [${arr}]`;

      return;
    }

    if (btn.innerText.toLowerCase() === "unshift") {
      arr.unshift(value);
      document.querySelector(
        ".display_insert_array"
      ).innerText = `Array:- [${arr}]`;

      return;
    }
  });
});

// *** Insert Value at nth position *** //

// WAY-1
let arr1 = [23, 43, 1, 21, 8, 4];

let indexInsertBtn = document.querySelector(".position_insert_btn");

function handleNthValueInsertion(e) {
  e.preventDefault();

  let elementValue = document.querySelector(".position_insert_value").value;
  let indexValue = document.querySelector(".position_index").value;

  if (elementValue === "" || indexValue === "") {
    alert("All fields are mandatory!");
    return;
  }

  elementValue = parseInt(elementValue);
  indexValue = parseInt(indexValue);

  // Way-1 - Using Spice() Method
  arr1.splice(indexValue, 0, elementValue);

  // Way-2 - Using Iteration Method
  // for (let i = arr1.length - 1; i >= indexValue; i--) {
  //   if (i >= indexValue) {
  //     arr1[i + 1] = arr1[i];
  //   }
  //   if (i === indexValue) {
  //     arr1[i] = elementValue;
  //   }
  // }

  document.querySelector(
    ".position_array_display"
  ).innerText = `Array:- [${arr1}]`;

  document.querySelector(".position_insert_value").value = "";
  document.querySelector(".position_index").value = "";
}
